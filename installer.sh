#!/bin/bash

# Use First Parameter as Db Username or Use Default
db_root_user="${1-admin}"
# Use Second Parameter as Db Password or Use Default
db_root_password="${2-admin}"
# Use Third Parameter as Db Name or Use Default
db_root_database_name="${3-wordpress}"

# Predicate that returns exit status 0 if the database root password
# is set, a nonzero exit status otherwise.
is_mysql_root_password_set() {
  ! mysqladmin --user=root status > /dev/null 2>&1
}

# Predicate that returns exit status 0 if the mysql(1) command is available,
# nonzero exit status otherwise.
is_mysql_command_available() {
  which mysql > /dev/null 2>&1
}

echo "######################################################################"
echo "Install Necessary Package."
echo "######################################################################"
# Update Package Index
sudo apt update

# Install Apache2, Maria, PHP, Phpmyadmin
sudo apt-get -yq install apache2 mariadb-server php php-mysql libapache2-mod-php php-cli 
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install phpmyadmin php-mbstring php-gettext
sudo phpenmod mbstring
sudo mv /usr/share/phpmyadmin /var/www/html/phpmyadmin
sudo systemctl restart apache2

echo "######################################################################"
echo "Securing Database."
echo "######################################################################"
# Predicate that returns exit status 0 if the database root password
# is set, a nonzero exit status otherwise.
is_mysql_root_password_set() {
  ! mysqladmin --user=root status > /dev/null 2>&1
}

# Predicate that returns exit status 0 if the mysql(1) command is available,
# nonzero exit status otherwise.
is_mysql_command_available() {
  which mysql > /dev/null 2>&1
}

if ! is_mysql_command_available; then
  echo "The MariaDB client mysql(1) is not installed."
  exit 1
fi

if is_mysql_root_password_set; then
  echo "Database root password already set"
  exit 0
fi

#add admin user
sudo mysql --user=root <<_EOF_
  CREATE USER '${db_root_user}'@'localhost' IDENTIFIED BY '${db_root_password}';
  GRANT ALL PRIVILEGES ON * . * TO '${db_root_user}'@'localhost';
  FLUSH PRIVILEGES;
_EOF_

#remove root user and create databse
sudo mysql --user=$db_root_user --password=$db_root_password<<_EOF_
  DELETE FROM mysql.user WHERE User='';
  DELETE FROM mysql.user WHERE User='root';
  DROP DATABASE IF EXISTS test;
  DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
  CREATE DATABASE ${db_root_database_name};
_EOF_

echo "######################################################################"
echo "Installing Wordpress."
echo "######################################################################"
#download wordpress
curl -O https://wordpress.org/latest.tar.gz

#unzip wordpress
tar -zxvf latest.tar.gz

#setup wordpress
sudo mv -v wordpress/* /var/www/html/  
cd /var/www/html/       
sudo rm index.html  
sudo cp wp-config-sample.php wp-config.php
sudo sed -i "s/database_name_here/$db_root_database_name/" wp-config.php
sudo sed -i "s/username_here/$db_root_user/" wp-config.php
sudo sed -i "s/password_here/$db_root_password/" wp-config.php

echo "######################################################################"
echo "Installation is complete."
echo "######################################################################"